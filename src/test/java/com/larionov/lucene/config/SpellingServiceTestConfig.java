package com.larionov.lucene.config;

import com.larionov.lucene.service.spelling.SpellingServiceImplTest;
import com.larionov.lucene.service.spelling.provider.FileDictionaryProvider;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(
        value = "com.larionov.lucene.service",
        excludeFilters = {@ComponentScan.Filter(classes = {FileDictionaryProvider.class}, type = FilterType.ASSIGNABLE_TYPE)}
)
public class SpellingServiceTestConfig extends ApplicationConfig{
    @Bean
    public SpellingServiceImplTest.DictionaryProviderMock dictionaryProvider() {
        return new SpellingServiceImplTest.DictionaryProviderMock();
    }
}
