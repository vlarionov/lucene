package com.larionov.lucene.service.spelling;

import com.larionov.lucene.config.SpellingServiceTestConfig;
import com.larionov.lucene.service.spelling.provider.AbstractDictionaryProvider;
import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpellingServiceTestConfig.class})
public class SpellingServiceImplTest {
    @Autowired
    private SpellingServiceImpl spellingService;
    @Autowired
    private DictionaryProviderMock dictionaryProvider;

    private Map<String, String> expectedSpellCheckResults;

    @Before
    public void setUp() {
        Set<String> sourceText = dictionaryProvider.getSourceText();
        sourceText.add("Два слова");
        sourceText.add("Три слова  пробелы   ");
        spellingService.updateIndex();
        expectedSpellCheckResults = new HashMap<>();
        expectedSpellCheckResults.put("слова", null);
        expectedSpellCheckResults.put("пробелы", null);
        expectedSpellCheckResults.put("дв","два");
        expectedSpellCheckResults.put("слава", "слова");
        expectedSpellCheckResults.put("три сова", "три слова");
        expectedSpellCheckResults.put("проблы", "пробелы");
        expectedSpellCheckResults.put("слова проб", "слова  пробелы");
        expectedSpellCheckResults.put("ва слова", "два слова");
        expectedSpellCheckResults.put("три слва  пробелы", "три слова  пробелы");
    }
    @Test
    public void thatSpellCheckCorrect() {
        Map<String, String> actualSpellCheckResults = new HashMap<>();
        Set<String> querySet = expectedSpellCheckResults.keySet();
        for (String query : querySet) {
            actualSpellCheckResults.put(
                    query,
                    spellingService.suggestSimilar(query.toLowerCase())
            );
        }
        assertEquals(expectedSpellCheckResults.entrySet(), actualSpellCheckResults.entrySet());
    }
    @Test
    public void whenIndexUpdatedThenNotFound() {
        dictionaryProvider.getSourceText().remove("Три слова  пробелы   ");
        spellingService.updateIndex();
        assertNull(spellingService.suggestSimilar("проблы"), null);
    }
    @Test
    public void whenIndexUpdatedThenFound() {
        dictionaryProvider.getSourceText().remove("Три слова  пробелы   ");
        spellingService.updateIndex();
        assertEquals(spellingService.suggestSimilar("дв"), "два");
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenQueryIsNull() {
        spellingService.suggestSimilar(null);
    }

    public static class DictionaryProviderMock extends AbstractDictionaryProvider {
        private final Set<String> sourceText = new HashSet<>();

        @Override
        protected Set<String> getSourceText() {
            return sourceText;
        }
    }

}