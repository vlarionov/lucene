package com.larionov.lucene.service.spelling.provider;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashSet;
import java.util.Set;

@Component
public class FileDictionaryProvider extends AbstractDictionaryProvider {
    private static final String FILE_NAME = "data.txt";

    @Override
    protected Set<String> getSourceText() {
        Set<String> result = new LinkedHashSet<>();
        InputStream fileStream = this.getClass().getClassLoader().getResourceAsStream(FILE_NAME);
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileStream))) {
            for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
                result.add(line);
            }
            return result;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
