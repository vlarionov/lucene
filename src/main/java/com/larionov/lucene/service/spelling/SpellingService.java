package com.larionov.lucene.service.spelling;

import com.sun.istack.internal.NotNull;

public interface SpellingService {
    void updateIndex();

    String suggestSimilar(@NotNull String query);

    class SpellCheckingException extends RuntimeException {
        public SpellCheckingException(Exception e) {
            super(e);
        }
    }
}
