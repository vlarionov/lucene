package com.larionov.lucene.service.spelling.provider;

import org.apache.lucene.search.spell.Dictionary;

public interface DictionaryProvider {
    Dictionary createDictionary();

    class DictionaryCreationException extends RuntimeException {
        public DictionaryCreationException(Exception e) {
            super(e);
        }
    }
}
