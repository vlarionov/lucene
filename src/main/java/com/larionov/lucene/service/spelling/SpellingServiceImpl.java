package com.larionov.lucene.service.spelling;

import com.larionov.lucene.service.spelling.provider.DictionaryProvider;
import com.sun.istack.internal.NotNull;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;

@Service
public class SpellingServiceImpl implements SpellingService {
    @Autowired
    private SpellChecker spellChecker;
    @Autowired
    private DictionaryProvider dictionaryProvider;
    @Autowired
    @Qualifier("spellCheckerAnalyzer")
    private Analyzer spellCheckerAnalyzer;

    public void updateIndex() {
        try {
            Directory newDirectory = new RAMDirectory();
            new SpellChecker(newDirectory).indexDictionary(
                    dictionaryProvider.createDictionary(),
                    new IndexWriterConfig(spellCheckerAnalyzer),
                    false
            );
            spellChecker.setSpellIndex(newDirectory);
        } catch (IOException e) {
            throw new SpellCheckingException(e);
        }
    }

    public String suggestSimilar(@NotNull String query) {
        Assert.notNull(query);
        try {
            String[] result = spellChecker.suggestSimilar(query.toLowerCase(), 1);
            return result.length > 0 ? result[0] : null;
        } catch (IOException e) {
            throw new SpellCheckingException(e);
        }
    }
}
