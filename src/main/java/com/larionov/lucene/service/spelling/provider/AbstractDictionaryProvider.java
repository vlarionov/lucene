package com.larionov.lucene.service.spelling.provider;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.spell.Dictionary;
import org.apache.lucene.search.spell.LuceneDictionary;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.IOException;
import java.util.*;

public abstract class AbstractDictionaryProvider implements DictionaryProvider {
    @Autowired
    @Qualifier("dictionaryAnalyzer")
    private Analyzer dictionaryAnalyzer;

    protected abstract Set<String> getSourceText();

    public Dictionary createDictionary() {
        Directory dictionaryDirectory = new RAMDirectory();
        try (IndexWriter indexWriter = new IndexWriter(dictionaryDirectory, new IndexWriterConfig(dictionaryAnalyzer))) {
            for (String line : getPreparedText()) {
                indexWriter.addDocument(Collections.singletonList(new StringField("line", line, Field.Store.YES)));
            }
            indexWriter.commit();
            IndexReader indexReader = DirectoryReader.open(dictionaryDirectory);
            return new LuceneDictionary(indexReader, "line");
        } catch (IOException e) {
            throw new DictionaryCreationException(e);
        }
    }

    private Set<String> getPreparedText() {
        Set<String> result = getSourceText();
        result = toLowerCase(result);
        result = splitFromOneSide(result, true);
        result = splitFromOneSide(result, false);
        return result;
    }

    private Set<String> toLowerCase(Set<String> source) {
        Set<String> result = new HashSet<>();
        source.forEach(str -> result.add(str.toLowerCase()));
        return result;
    }

    private Set<String> splitFromOneSide(Set<String> text, boolean isRightSize) {
        Set<String> result = new HashSet<>();
        for (String line : text) {
            StringBuilder lineBuilder = new StringBuilder(line);
            StringTokenizer tokenizer = new StringTokenizer(lineBuilder.toString());
            List<String> tokens = new ArrayList<>();
            while (tokenizer.hasMoreTokens()) {
                tokens.add(tokenizer.nextToken());
            }
            if (!isRightSize) {
                Collections.reverse(tokens);
            }
            for (String token : tokens) {
                int tokenIndex = isRightSize ? lineBuilder.indexOf(token) : lineBuilder.lastIndexOf(token);
                int deleteFrom = isRightSize ? 0 : tokenIndex;
                int deleteTo = isRightSize ? tokenIndex + token.length() : lineBuilder.length();
                result.add(lineBuilder.toString().trim());
                lineBuilder.delete(deleteFrom, deleteTo);
            }
        }
        return result;
    }

}
