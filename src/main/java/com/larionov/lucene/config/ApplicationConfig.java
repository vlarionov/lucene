package com.larionov.lucene.config;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
@ComponentScan("com.larionov.lucene.service")
public class ApplicationConfig {
    @Bean
    public SpellChecker spellChecker() {
        try {
            Directory directory = new RAMDirectory();
            SpellChecker spellChecker = new SpellChecker(directory);
            spellChecker.setAccuracy(0.6f);
            return spellChecker;
        } catch (IOException e) {
            throw new BeanCreationException("SpellChecker creation error", e);
        }
    }

    @Bean
    public Analyzer spellCheckerAnalyzer() {
        return new RussianAnalyzer();
    }

    @Bean
    public Analyzer dictionaryAnalyzer() {
        return new RussianAnalyzer();
    }
}
