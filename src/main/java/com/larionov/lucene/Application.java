package com.larionov.lucene;

import com.larionov.lucene.config.ApplicationConfig;
import com.larionov.lucene.service.spelling.SpellingService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Application {
    private static final ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);

    public static void main(String[] args) {
        SpellingService spellingService = applicationContext.getBean(SpellingService.class);
        spellingService.updateIndex();
        String inputString;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            do {
                print("Search:");
                inputString = bufferedReader.readLine();
                print("Results:");
                print(spellingService.suggestSimilar(inputString));
                print("");
            } while (!"exit".equals(inputString));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private static void print(String string) {
        System.out.println(string);
    }
}
